package de.reibner.untexify;

import de.reibner.untexify.exceptions.EncodingDetectionException;
import de.reibner.untexify.exceptions.FailedToCreateFileException;
import de.reibner.untexify.exceptions.FailedToOpenFileException;
import org.apache.any23.encoding.TikaEncodingDetector;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Observable;

class FileUnTexifyer extends Observable {


    private List<File> files;
    private File saveLocation;
    FileUnTexifyer(List<File> files, File saveLocation){
        this.saveLocation = saveLocation;
        this.files = files;
    }

    void untexify() throws FailedToOpenFileException, FailedToCreateFileException, EncodingDetectionException {
        for(File file :files){
            if(file.isFile() ){
                String fileContent = openFile(file);
                String untexifiedString = untexifyString(fileContent);
                createNewFile(file, untexifiedString);
            }
            super.setChanged();
            super.notifyObservers(null);
        }
    }

    private String openFile(File file) throws FailedToOpenFileException{
        StringBuilder sb = new StringBuilder();

        try(BufferedReader reader = new BufferedReader(new FileReader(file.getAbsolutePath()))){
            String line;
            while ((line = reader.readLine()) != null){
                sb.append(line);
                sb.append(System.getProperty("line.separator"));
            }
        } catch (IOException e) {
            throw new FailedToOpenFileException("Failed to open the file: "+file.getAbsolutePath(), e);
        }

        return sb.toString();
    }

    private String untexifyString(String string){
        String untexifiedString = string.replaceAll("\\\\[^()\\[\\]{}\\s]+(\\(.*?\\))*+(\\[.*?])?+(\\{.+?})?+", ""); //remove all LaTex calls
        untexifiedString = untexifiedString.replaceAll("%.*", ""); //remove all LaTex comments
        untexifiedString = untexifiedString.replaceAll("''", "\""); //replace LaTex quotation marks with real quotation marks
        return untexifiedString;
    }

    private void createNewFile(File originalFile, String fileContent) throws FailedToCreateFileException, EncodingDetectionException {
        File newFile = new File(saveLocation.getAbsolutePath()+File.separator+originalFile.getName() + ".untex");
        try {

            FileUtils.writeStringToFile(newFile, fileContent, guessCharset(new FileInputStream(originalFile)));
        } catch (IOException e) {
            throw new FailedToCreateFileException(String.format("Failed to create new file with the name: %s in directory %s", newFile.getName(), saveLocation.getAbsolutePath()), e);
        }
    }

    private Charset guessCharset(InputStream is) throws EncodingDetectionException {
        try {
            return Charset.forName(new TikaEncodingDetector().guessEncoding(is));
        } catch (IOException e) {
            throw new EncodingDetectionException("Something went wrong while trying to detect the encoding of the file", e);
        }
    }
}
