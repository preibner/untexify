package de.reibner.untexify;

import de.reibner.untexify.exceptions.EncodingDetectionException;
import de.reibner.untexify.exceptions.FailedToCreateFileException;
import de.reibner.untexify.exceptions.FailedToOpenFileException;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static de.reibner.untexify.util.WindowSize.HEIGHT;
import static de.reibner.untexify.util.WindowSize.WIDTH;

public class UnTexify extends Application {

    private Label chosenFile;
    private Label chosenFileLabel;
    private Button chooseFileButton;
    private Button untexifyButton;
    private Label saveLocation;
    private Label saveLocationLabel;
    private Button chooseSaveLocationButton;

    private List<File> filesToUntexify = new ArrayList<>();
    private File saveLocationFile;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("UnTexify!");

        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER_LEFT);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25,25,25,25));

        Scene scene = new Scene(grid, WIDTH, HEIGHT);
        primaryStage.setScene(scene);

        chosenFileLabel = new Label("Chosen file:");
        chosenFile = new Label("No file chosen yet");
        chosenFile.setWrapText(true);
        chooseFileButton = new Button("Choose file");

        saveLocation = new Label("No save location chosen yet");
        saveLocationLabel = new Label("Chosen save location");
        chooseSaveLocationButton = new Button("Choose save location");

        untexifyButton = new Button("UnTexify!");

        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("LaTex files", "*.tex");
        fileChooser.getExtensionFilters().add(filter);

        DirectoryChooser saveLocationChooser = new DirectoryChooser();

        chooseFileButton.setOnAction(
                (event) -> {
                    filesToUntexify = fileChooser.showOpenMultipleDialog(primaryStage);
                    StringBuilder sb = new StringBuilder();
                    for(File file : filesToUntexify){
                        sb.append(file.toString());
                        sb.append(';');
                    }
                    sb.setLength(sb.length()-1); //remove last ;
                    chosenFile.setText(sb.toString());
                }
        );

        chooseSaveLocationButton.setOnAction(
                event -> {
                    saveLocationFile = saveLocationChooser.showDialog(primaryStage);
                    saveLocation.setText(saveLocationFile.toString());
                }
        );

        untexifyButton.setOnAction(
                event -> {
                    FileUnTexifyer fileUnTexifyer = new FileUnTexifyer(filesToUntexify, saveLocationFile);
                    ProgressBarScene pb = new ProgressBarScene(WIDTH, HEIGHT, filesToUntexify.size());
                    primaryStage.getScene().setRoot(pb);
                    fileUnTexifyer.addObserver(pb);
                    try {
                        fileUnTexifyer.untexify();
                    } catch (FailedToOpenFileException | FailedToCreateFileException | EncodingDetectionException e) {
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setContentText(e.getMessage());
                    }
                }
        );






        grid.add(chosenFileLabel, 0, 0);
        grid.add(chosenFile, 1, 0);
        grid.add(chooseFileButton, 2, 0);
        grid.add(saveLocationLabel, 0, 1);
        grid.add(saveLocation, 1, 1);
        grid.add(chooseSaveLocationButton, 2, 1);
        grid.add(untexifyButton, 0, 2);

        primaryStage.setTitle("UnTexify!");
        primaryStage.show();
    }
}
