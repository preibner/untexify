package de.reibner.untexify.exceptions;

public class EncodingDetectionException extends Exception {
    public EncodingDetectionException() {
    }

    public EncodingDetectionException(String message) {
        super(message);
    }

    public EncodingDetectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public EncodingDetectionException(Throwable cause) {
        super(cause);
    }

    public EncodingDetectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
