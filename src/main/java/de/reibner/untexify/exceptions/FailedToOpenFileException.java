package de.reibner.untexify.exceptions;

public class FailedToOpenFileException extends Exception {
    public FailedToOpenFileException() {
    }

    public FailedToOpenFileException(String message) {
        super(message);
    }

    public FailedToOpenFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public FailedToOpenFileException(Throwable cause) {
        super(cause);
    }

    public FailedToOpenFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
