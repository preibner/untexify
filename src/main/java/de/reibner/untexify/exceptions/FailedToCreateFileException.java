package de.reibner.untexify.exceptions;

public class FailedToCreateFileException extends Exception {
    public FailedToCreateFileException() {
    }

    public FailedToCreateFileException(String message) {
        super(message);
    }

    public FailedToCreateFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public FailedToCreateFileException(Throwable cause) {
        super(cause);
    }

    public FailedToCreateFileException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
