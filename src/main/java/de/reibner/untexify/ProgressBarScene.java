package de.reibner.untexify;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.util.Observable;
import java.util.Observer;

import static de.reibner.untexify.util.WindowSize.HEIGHT;
import static de.reibner.untexify.util.WindowSize.WIDTH;

public class ProgressBarScene extends Pane implements Observer {

    private double size;
    private double progress = 0;

    private ProgressBar progressBar;
    private Label progressLabel;
    private Label readyLabel;
    private GridPane grid = new GridPane();

    ProgressBarScene(double width, double height, double hundredPercent){
        this.size = hundredPercent;
        progressBar = new ProgressBar(hundredPercent);
        progressLabel = new Label("Progress");
        readyLabel = new Label("Finished!");
        readyLabel.setVisible(false);
        grid.setAlignment(Pos.CENTER_LEFT);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25,25,25,25));


        grid.add(progressLabel, 0, 0);
        grid.add(progressBar, 1,0);
        grid.add(readyLabel, 2, 0);
    }

    void addOneToProgress(){
        progress++;
        update();
    }

    private void update(){
        double progressPercent = progress/size;
        progressBar.setProgress(progressPercent);

        if(progressPercent >= 1){
            readyLabel.setVisible(true);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        addOneToProgress();
    }
}
